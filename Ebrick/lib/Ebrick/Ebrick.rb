MAX_EOL = 2
ROOT_PATH = '/usr/src/app/Ebrick/lib/files'

require 'socket'
require 'response'
require 'request'

module Ebrick
  class Server
    def initialize(app)
      @app = app
    end

    def start
        socket = TCPServer.new(ENV['HOST'], ENV['PORT'])

        puts "Listening on #{ENV['HOST']}:#{ENV['PORT']}. Press CTRL+C to cancel."

        loop do
            Thread.start(socket.accept) do |client|
            handle_connection(client)
            end
        end
    end

    def shutdown; end

    private

    def handle_request(request_text, client)
        request = ::Ebrick::Request.new(request_text)
        puts "#{client.peeraddr[3]} #{request.path}"

        file_path = File.join(ROOT_PATH, request.path)
        response = if !File.file?(file_path)
                        ::Ebrick::Response.new(code: 204)
                    else File.readable?(file_path)
                        file = File.open(file_path, 'rb')
                        mime_type = "file -b --mime-type #{file_path}.chomp"
                        headers = ["Content-Type: #{mime_type}"]
                        ::Ebrick::Response.new(code: 200, data: file.read)
                    end

        response.send(client)

        client.shutdown
    end

    def handle_connection(client)
        puts "Getting new client #{client}"
        request_text = ''
        eol_count = 0

        loop do
            buf = client.recv(1)
            request_text += buf

            eol_count += 1 if buf == "\n"

            if eol_count == MAX_EOL
            handle_request(request_text, client)
            break
            end

        end
    rescue Errno::EPERM => e
    puts "Error: #{e}"

    response = ::Ebrick::Response.new(code: 403, data: "Operation not permitted\n")
    response.send(client)
    rescue => e
    puts "Error: #{e}"

    response = ::Ebrick::Response.new(code: 500, data: "Internal Server Error\n")
    response.send(client)
    ensure
    client.close
    end
  end
end